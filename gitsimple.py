import subprocess
from typing import List, Optional

import typer

app = typer.Typer()


def cmd(args: List[str]) -> None:
    typer.echo(typer.style(f"$ {' '.join(args)}", fg="bright_cyan"))
    subprocess.run(args)


@app.callback()
def callback():
    """
    Do everything you need to do with git, intuitively.
    """


@app.command()
def info() -> None:
    """
    Get information on the state of this repository.
    """
    cmd(["git", "pull"])
    cmd(["git", "status"])


@app.command()
def save() -> None:
    """
    Save all of your current changes.
    """
    cmd(["git", "fetch"])
    cmd(["git", "diff"])
    cmd(["git", "branch"])
    msg = typer.prompt("Enter your commit message")
    if msg.strip() == "":
        typer.Exit(code=1)
    cmd(["git", "add", "-A", "."])
    cmd(["git", "commit", "-m", msg])
    cmd(["git", "push"])


@app.command()
def go(branch: str) -> None:
    """
    Go to the given branch name.
    """
    cmd(["git", "fetch"])
    cmd(["git", "checkout", branch])


if __name__ == "__main__":
    app()
