# git-simple

Do everything you need to do with some extra intuitive git subcommands.

## Commands

### `git info`: get info about this repository

1. fetches remote changes
1. shows list of changed files and branch info

### `git save`: save your changes

1. fetches remote changes
1. view diff for review
1. enter a commit message to add all changes and commit or leave empty to cancel
1. pushes changes to remote

### `git go [branch]`: move to a different branch or create a new one

1. fetches remote changes
1. checks if you have unsaved changes and if you do, prompts you to save them.
1. moves to named branch or creates it if it doesn't exist (asks for confirmation)

## Development

- `brew install asdf pre-commit`
- run `poetry shell` to enter project's vm
- run `poetry install` to install python dependencies
- run `poetry build` to build locally
- run `pipx install --force dist/gitsimple-0.1.1.tar.gz` to install locally

